# **Tutorial git command yang sering dipakai**

## create new branch from existing branch
```
git checkout -b <new-branch-name> <existing-branch>
git push origin <new-branch-name>
```

## merging main (modified) with other branch - task-1 (modified)
See [Reference](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging).
```
git checkout main
git merge task-1
```
## Delete Local Branch
```
$ git branch -d <branch_name>
$ git branch -D <branch_name>
```

- The -d option is an alias for --delete, which only deletes the branch if it has already been fully merged in its upstream branch.

- The -D option is an alias for --delete --force, which deletes the branch "irrespective of its merged status." [Source: man git-branch]

- You will receive an error if you try to delete the currently selected branch.

## Delete Remote Branch
```
git push -d <remote_name> <branchname>
```
Note: In most cases, <remote_name> will be origin.

## rebase main from branch task-1
rebase digunakan jika branch utama atau branch sumber tidak ada perubahan
```
git checkout main
git rebase task-1
git status
git push origin main
```
## rebase branch fitur-b from main
rebase digunakan jika branch utama ada perubahan contoh untuk perbaikan bug dan branch fitur-b sudah di push ke branch nya\
[Sumber](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)
```
git checkout fitur-b
git rebase main
git checkout main
git merge fitur-b 
git push origin main
```
**hasil akhir akan menyamakan main dengan fitur-b dan menjadi garis lurus tanpa menambahkan titik merge**\
**origin/fitur-b akan tertinggal dan masih membuat cabang.** \
(Optional)
1. Delete Branch jika benar-benar tidak di gunakan lagi local atau remote
2. susun ulang fitur-b dengan origin/fitur-b jika masih ingin digunakan, terutama menambahkan sesuatu yang kurang
```
git checkout fitur-b
git pull --rebase origin fitur-b 
git push origin fitur-b
```
command to cancel rebase\
`git rebase --abort`

## Untuk kembali ke commit tertentu (tanpa mengubah file apa pun)
`git checkout <sha1>`\
contoh sha1 -> a7a4daebe3b0cebb1034411c6813b88af21dc38f


undo commit before push \
git reset HEAD~1

rename branch locally \
git branch -m <oldname> <newname> \
or \
git branch -m <newname>

If you want to push the local branch and reset the upstream branch (actualy will create new branch) \
git push origin -u <newname>

And finally if you want to Delete the remote branch \
git push origin --delete <oldname> 

Renaming a local Git Branch is a matter of running a single command. \
However, you can’t directly rename a remote branch; \
you need to push the renamed local branch and delete the branch with the old name.


git reset --hard "HEAD@{7}" \
git log --all --oneline --graph 
